# Zrim Utils

## Introduction

Contains node.js utility functions

## Function Utilities

See [function utils](documentations/function-utils.md)

## Class Utilities

See [class utilities](documentations/class-utils.md)

