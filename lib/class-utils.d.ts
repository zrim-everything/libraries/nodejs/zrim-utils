/**
 * Contains utilities for classes
 */
export class ClassUtils {

  /**
   * Test if the value has the method name
   * @param {Object} value The object
   * @param {string} name The name
   * @return {boolean} true if has the method, otherwise false
   */
  public hasMethod(value: Object, name: string): boolean;

  /**
   * Extracts the instance method names. This is useful to extact method from instance created with a class syntax
   * @param {Object} value The value on which to extract the method names
   * @param {Object} [stopOnPrototype] A possible prototype where to stop the looking
   * @return {string[]} The method names found
   */
  public getInstanceMethodNames(value: Object, stopOnPrototype?: Object): string[];

  /**
   * Get the class name from the instance
   * @param {Function|Object} instance The instance
   * @throws {Error} In cas the instance is invalid
   */
  public getClassName(instance: Object | Function): string;
}

export declare const utils: ClassUtils;
