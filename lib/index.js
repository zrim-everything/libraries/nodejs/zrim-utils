
module.exports.JavaScriptHelper = exports.JavaScriptHelper = require('./js-helper').JavaScriptHelper;
module.exports.javaScriptHelper = exports.javaScriptHelper = require('./js-helper').javaScriptHelper;

module.exports.classUtils = exports.classUtils = require('./class-utils').utils;
module.exports.ClassUtils = exports.ClassUtils = require('./class-utils').ClassUtils;

module.exports.functionUtils = exports.functionUtils = require('./function-utils');
