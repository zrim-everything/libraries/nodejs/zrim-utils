const functionUtils = require('./function-utils'),
  _ = require('lodash');

class ClassUtils {

  // See http://code.fitness/post/2016/01/javascript-enumerate-methods.html
  hasMethod(value, name) {
    if (!value || (typeof value !== 'object') || typeof name !== 'string') {
      return false;
    }

    const desc = Reflect.getOwnPropertyDescriptor(value, name);
    return !!desc && typeof desc.value === 'function';
  }

  // See http://code.fitness/post/2016/01/javascript-enumerate-methods.html
  getInstanceMethodNames(value, stopOnPrototype) {
    if (!value || typeof value !== 'object') {
      return [];
    }

    const names = [];
    // First get own properties
    Object.getOwnPropertyNames(value)
      .forEach(name => {
        if (name !== 'constructor' && this.hasMethod(value, name)) {
          names.push(name);
        }
      });

    let proto = Reflect.getPrototypeOf(value);

    while (proto && proto !== stopOnPrototype) {
      Object.getOwnPropertyNames(proto)
        .forEach(name => {
          if (name !== 'constructor' && this.hasMethod(proto, name)) {
            names.push(name);
          }
        });
      proto = Reflect.getPrototypeOf(proto);
    }

    return _.uniq(names);
  }

  getClassName(instance) {
    if (typeof instance === 'function') {
      return functionUtils.extractFunctionNameFromFunction(instance);
    } else if (typeof instance === 'object') {
      return functionUtils.extractFunctionNameFromInstance(instance);
    } else {
      throw new TypeError('Invalid instance type');
    }
  }
}

exports.utils = module.exports.utils = new ClassUtils();
exports.ClassUtils = module.exports.ClassUtils = ClassUtils;
