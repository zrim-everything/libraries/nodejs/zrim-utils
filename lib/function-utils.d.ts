/**
 * Extract the function name from the string (after doing a function() {}.toString()
 * @param {string} str The string value
 * @return {string} The name if found, otherwise undefined
 */
export function extractFunctionNameFromString(str: string): string | undefined;

/**
 * Extract the function name
 * @param {Function} fn The function
 * @return {string} The function name in case the extract succeed, otherwise empty string
 */
export function extractFunctionNameFromFunction(fn: Function): string;

/**
 * Try to extract the function name from the instance, After doing a new
 * @param {Object} instance The instance object
 * @return {string} The class name in case the extract succeed, otherwise empty string
 * @see extractFunctionNameFromFunction
 */
export function extractFunctionNameFromInstance(instance: Object): string;
