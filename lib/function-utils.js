

function extractFunctionNameFromString(str) {
  if (typeof str !== 'string') {
    return undefined;
  }

  // Match:
  // - ^          the beginning of the string
  // - function   the word 'function'
  // - \s+        at least some white space
  // - ([\w\$]+)  capture one or more valid JavaScript identifier characters
  // - \s*        optionally followed by white space (in theory there won't be any here,
  //              so if performance is an issue this can be omitted[1]
  // - \(         followed by an opening brace
  //
  const result = /^function\s+([\w$]*)\s*\(/.exec(str) || /^class\s+([\w$]*)\s*{/.exec(str);

  return result ? result[1] : undefined;
}


function extractFunctionNameFromFunction(fn) {
  if (typeof fn === 'function') {
    if (fn.name) {
      return fn.name;
    } else {
      const name = exports.extractFunctionNameFromString(fn.toString());
      return name ? name : '';
    }
  }

  return '';
}


function extractFunctionNameFromInstance(instance) {
  if (typeof instance !== 'object') {
    return '';
  }

  // I do not know the case when constructor is not defined :s
  return exports.extractFunctionNameFromFunction(instance.constructor);
}

module.exports.extractFunctionNameFromString = exports.extractFunctionNameFromString = extractFunctionNameFromString;
module.exports.extractFunctionNameFromFunction = exports.extractFunctionNameFromFunction = extractFunctionNameFromFunction;
module.exports.extractFunctionNameFromInstance = exports.extractFunctionNameFromInstance = extractFunctionNameFromInstance;
