

export {ClassUtils} from './class-utils'

import {ClassUtils} from './class-utils'
export declare const classUtils: ClassUtils;

import * as namespace_functions from './function-utils';
export import functionUtils = namespace_functions;
