const SimpleTestLauncher = require('zrim-test-bootstrap').SimpleTestLauncher,
  SimpleTestLauncherConfigBuilder = require('zrim-test-bootstrap').configs.SimpleTestLauncherConfigBuilder;

const configBuilder = new SimpleTestLauncherConfigBuilder();

configBuilder
  .projectConfiguration()
  .rootDirectoryPath(__dirname + '/../..')
  .parentBuilder()
  .testConfiguration()
  .unitTest()
  .parentBuilder()
  .coverageConfiguration()
  .enableSummary(true)
  .parentBuilder();

SimpleTestLauncher.execute(configBuilder.build());
