describe('#ClassUtils', function () {
  const ClassUtils = require('../../../lib/class-utils').ClassUtils;

  function createInstance() {
    return new ClassUtils();
  }

  describe('#hasMethod', function () {
    it("Given invalid value Then must return false", function () {
      const instance = createInstance();

      expect(instance.hasMethod(undefined, 'a')).toBe(false);
      expect(instance.hasMethod(null, 'a')).toBe(false);
      expect(instance.hasMethod(12, 'a')).toBe(false);
      expect(instance.hasMethod('d', 'a')).toBe(false);
      expect(instance.hasMethod('', 'a')).toBe(false);
      expect(instance.hasMethod(false, 'a')).toBe(false);
      expect(instance.hasMethod(true, 'a')).toBe(false);
    });

    it("Given invalid name Then must return false", function () {
      const instance = createInstance();

      expect(instance.hasMethod({}, undefined)).toBe(false);
      expect(instance.hasMethod({}, null)).toBe(false);
      expect(instance.hasMethod({}, 12)).toBe(false);
      expect(instance.hasMethod({}, false)).toBe(false);
      expect(instance.hasMethod({}, true)).toBe(false);
      expect(instance.hasMethod({}, {})).toBe(false);
    });

    it("Given property not exists Then must return false", function () {
      const instance = createInstance();

      expect(instance.hasMethod({}, 'cv')).toBe(false);
    });

    it("Given property exists but not a function Then must return false", function () {
      const instance = createInstance();

      const obj = {
        k: 12
      };
      expect(instance.hasMethod(obj, 'k')).toBe(false);
    });

    it("Given property exists and a function Then must return true", function () {
      const instance = createInstance();

      const obj = {
        k: function () {

        }
      };
      expect(instance.hasMethod(obj, 'k')).toBe(true);
    });
  }); // #hasMethod

  describe('#getInstanceMethodNames', function () {
    it("Given not object as value Then must return empty array", function () {
      const instance = createInstance();

      expect(instance.getInstanceMethodNames(undefined)).toEqual([]);
      expect(instance.getInstanceMethodNames(null)).toEqual([]);
      expect(instance.getInstanceMethodNames(12)).toEqual([]);
      expect(instance.getInstanceMethodNames(false)).toEqual([]);
      expect(instance.getInstanceMethodNames(true)).toEqual([]);
      expect(instance.getInstanceMethodNames('as')).toEqual([]);
    });

    it("Given function instance as value Then must return expected value", function () {
      const instance = createInstance();

      function bh() {

      }

      bh.prototype.po = function () { };

      const input = new bh();
      input.po = () => {};
      input.pi = () => {};
      input.oi = {};

      expect(instance.getInstanceMethodNames(input)).toEqual(jasmine.arrayContaining(
        Array.prototype.concat.apply([
          'po',
          'pi'
        ], Object.keys(function () {}))
      ));
    });

    it("Given class instance as value Then must return expected value", function () {
      const instance = createInstance();

      class Bh {
        po() {}
      }

      expect(instance.getInstanceMethodNames(new Bh())).toEqual(jasmine.arrayContaining(
        Array.prototype.concat.apply([
          'po'
        ], Object.keys(function () {}))
      ));
    });

    it("Given class instance (with inheritance) as value Then must return expected value", function () {
      const instance = createInstance();

      class Bh {
        po() {}
      }

      class Bo extends Bh {
        j() {}
      }

      expect(instance.getInstanceMethodNames(new Bo())).toEqual(jasmine.arrayContaining(
        Array.prototype.concat.apply([
          'po', 'j'
        ], Object.keys(function () {}))
      ));
    });
  }); // #getInstanceMethodNames

  describe('#getClassName', function () {
    const functionUtils = require('../../../lib/function-utils');

    it("Given function Then must return expected value", function () {
      const instance = createInstance();

      function abhn() {}
      spyOn(functionUtils, 'extractFunctionNameFromFunction').and.returnValue('abhdbsdf');

      expect(instance.getClassName(abhn)).toBe('abhdbsdf');
      expect(functionUtils.extractFunctionNameFromFunction).toHaveBeenCalledWith(abhn);
    });

    it("Given class Then must return expected value", function () {
      const instance = createInstance();

      class abhn {}
      spyOn(functionUtils, 'extractFunctionNameFromFunction').and.returnValue('abhdbsdf');

      expect(instance.getClassName(abhn)).toBe('abhdbsdf');
      expect(functionUtils.extractFunctionNameFromFunction).toHaveBeenCalledWith(abhn);
    });

    it("Given class instance Then must return expected value", function () {
      const instance = createInstance();

      class Ab {

      }
      const ab = new Ab();
      spyOn(functionUtils, 'extractFunctionNameFromInstance').and.returnValue('abhdbsdf');

      expect(instance.getClassName(ab)).toBe('abhdbsdf');
      expect(functionUtils.extractFunctionNameFromInstance).toHaveBeenCalledWith(ab);
    });

    it("Given invalid instance Then must throw error", function () {
      const instance = createInstance();

      expect(() => instance.getClassName('')).toThrow(jasmine.any(TypeError));
    });
  }); // #getClassName
}); // #ClassUtils
