describe('#functionUtils', function () {
  const functionUtils = require('../../../lib/function-utils');

  describe('#extractFunctionNameFromString', function () {
    const extractFunctionNameFromString = functionUtils.extractFunctionNameFromString;

    it("Given no string Then must return undefined", function () {
      expect(extractFunctionNameFromString({})).toBeUndefined();
      expect(extractFunctionNameFromString(12)).toBeUndefined();
    });

    it("Given string without function Then must return undefined", function () {
      expect(extractFunctionNameFromString('dfasdf')).toBeUndefined();
    });

    it("Given string with function Then must return expected value", function () {
      expect(extractFunctionNameFromString(function () {

      }.toString())).toBe('');

      expect(extractFunctionNameFromString(function abgh() {

      }.toString())).toBe('abgh');
    });

    it("Given string with class Then must return expected value", function () {
      expect(extractFunctionNameFromString(class {

      }.toString())).toBe('');

      expect(extractFunctionNameFromString(class abgh {

      }.toString())).toBe('abgh');
    });
  }); // #extractFunctionNameFromString

  describe('#extractFunctionNameFromFunction', function () {
    const extractFunctionNameFromFunction = functionUtils.extractFunctionNameFromFunction;

    it("Given no function Then must return empty string", function () {
      expect(extractFunctionNameFromFunction('')).toBe('');
      expect(extractFunctionNameFromFunction({})).toBe('');
      expect(extractFunctionNameFromFunction(false)).toBe('');
      expect(extractFunctionNameFromFunction(true)).toBe('');
      expect(extractFunctionNameFromFunction(12)).toBe('');
    });

    it("Given anonymous function and extractFunctionNameFromString returns undefined Then must return empty string", function () {
      spyOn(functionUtils, 'extractFunctionNameFromString');

      expect(extractFunctionNameFromFunction(function () {})).toBe('');
      expect(functionUtils.extractFunctionNameFromString).toHaveBeenCalledWith('function () {}');
    });

    it("Given anonymous function and extractFunctionNameFromString returns name Then must return expected value", function () {
      spyOn(functionUtils, 'extractFunctionNameFromString').and.returnValue('skslks');

      expect(extractFunctionNameFromFunction(function () {})).toBe('skslks');
      expect(functionUtils.extractFunctionNameFromString).toHaveBeenCalledWith('function () {}');
    });

    it("Given named function Then must return expected value", function () {
      spyOn(functionUtils, 'extractFunctionNameFromString');

      expect(extractFunctionNameFromFunction(function adfgg() {})).toBe('adfgg');
      expect(functionUtils.extractFunctionNameFromString).not.toHaveBeenCalled();
    });

    it("Given named class Then must return expected value", function () {
      spyOn(functionUtils, 'extractFunctionNameFromString');

      expect(extractFunctionNameFromFunction(class adfgg {})).toBe('adfgg');
      expect(functionUtils.extractFunctionNameFromString).not.toHaveBeenCalled();
    });
  }); // #extractFunctionNameFromFunction

  describe('#extractFunctionNameFromInstance', function () {
    const extractFunctionNameFromInstance = functionUtils.extractFunctionNameFromInstance;

    it("Given no object Then must return empty string", function () {
      expect(extractFunctionNameFromInstance('')).toBe('');
      expect(extractFunctionNameFromInstance(false)).toBe('');
      expect(extractFunctionNameFromInstance(true)).toBe('');
      expect(extractFunctionNameFromInstance(12)).toBe('');
      expect(extractFunctionNameFromInstance(function () {})).toBe('');
    });

    it("Given instance Then must return expected value", function () {
      spyOn(functionUtils, 'extractFunctionNameFromFunction').and.returnValue('saoshda');

      expect(extractFunctionNameFromInstance({})).toBe('saoshda');
      expect(functionUtils.extractFunctionNameFromFunction).toHaveBeenCalledWith(Object);
    });
  }); // #extractFunctionNameFromInstance
});
