# Class Utilities

```javascript
require('zrim-utils').classUtils
```

The class itself
```javascript
require('zrim-utils').ClassUtils
```

## Introduction

Contains utilities to handle javascript classes

## Methods

### getInstanceMethodNames

```
getInstanceMethodNames(value: Object, stopOnPrototype?: Object): string[]
```

Extract the method names from the given instance. It works with es5 prototype function
and es6 classes.


### getClassName

```
getClassName(instance: Object): string
```

Extract the class name from the instance.

This method throw an error if the instance has invalid type.

See [function utils](function-utils.md)
