# Functions Utilities

```javascript
require('zrim-utils').functionUtils
```

## Introduction

Contains utilities for function

## Methods

### extractFunctionNameFromString

```
extractFunctionNameFromString(str: string): string | undefined
```

Try to extract the function name from a string obtained with `function () {}.toString()`.
It also works with es6 class.

In case the name cannot be extract, the method return undefined. This help to know the difference
from an anonymous function/class and a not valid string

### extractFunctionNameFromFunction

```
extractFunctionNameFromFunction(fn: Function): string
```

Try to extract the function name from the input.
It also work with es6 class.

### extractFunctionNameFromInstance

```
extractFunctionNameFromInstance(instance: Object): string
```

Try to extract the function name from the instance obtained after a `new`.
